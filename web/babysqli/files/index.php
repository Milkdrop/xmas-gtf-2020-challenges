<?php
include ("config.php");
?>

<head>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<h1>EUROBEAT<small><i>//DATABASE</i></small></h1>
	<div id="container">
		<img id="image" src="https://upload.wikimedia.org/wikipedia/en/a/a6/InitialD_vol1_Cover.jpg">
		<div id="content">
			Search your TUNE~:
			<form action="/">
				<input name="search" type="text" value="<?php echo $_GET['search']?>">
				<button type="submit">探す</button>
			</form>
			<?php
				if (isset ($_GET['search'])) {
					$conn = new mysqli ($servername, $username, $password, $dbname);
					$result = mysqli_query ($conn, "SELECT * FROM tracks WHERE title LIKE '%" . $_GET['search'] . "%'", MYSQLI_STORE_RESULT);

					if ($result->num_rows === 0) {
						echo '<b style="color:#FF1930">No Results.</b>';
					} else {
						echo '<table>
						<tr>
							<td>Title</td>
							<td>Video</td>
						</tr>';

						$i = 1;

						foreach ($result as $row) {
							echo '<tr>
								<td><a class="track-title" target="_blank" href="https://youtube.com/watch?v=', $row['id'], '"><b>', $i, '. ', $row["title"], '</b></a></td>
								<td><a target="_blank" href="https://youtube.com/watch?v=', $row['id'], '"><img src="http://i3.ytimg.com/vi/', $row['id'], '/maxresdefault.jpg"></a></td>
							</tr>';

							$i += 1;
						}

						echo '</table>';
					}
				}
			?>
		</div>
	</div>
</body>