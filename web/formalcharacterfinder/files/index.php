<?php

if(!isset($_GET['name'])) {
    echo "Good day, sir!<br>Welcome to our formal character finder service.<br>Kindly input the name of your favorite character and we will display all the information we have about that character.<br>";
    echo "<form action='/' method='GET'>";
    echo "<input name='name' type='text'>";
    echo "<input type='submit' value='Query'>";
    echo "</form>";
} else {
    $name = $_GET['name'];
    $name = str_replace("%c", "X", $name);
    $name = str_replace("%x", "0x1337", $name);
    $name = str_replace("%p", "0x1337", $name);
    $name = str_replace("%s", "X-MAS{n3xt_t1me_try_xss_0n_b1nar1es_1_b3t_y0u_w1ll_b3_th3_f1rst_0ne}", $name);
    echo "Query: " . $name . "<br>";
    echo "Unfortunately, there were no results. Please accept our sincere apologies.<br>";
    echo "<a href='/'>Go back</a>";
}

?>