<!doctype html>
<html>
<head>
    <title>Question</title>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" style="margin-top:50px">
                <div class="panel-heading">
                    <h3 class="panel-title">A Simple Question</h3>
                </div>
                <div class="panel-body">
                    <div>
                        What is the answer?
                    </div>  
<?php
if(isset($_POST['answer'])) {
    echo '<h1>';
    if($_POST['answer'] === '42') {
        echo 'X-MAS{yakuhito_s_blog_1s_h00l00v00}';
    } else {
        echo 'Wrong';
    }
    echo '</h1><br>';
}
?>
                    <form action="/" method="POST">
<!-- source code is hidden this time -->
                        <fieldset>
                            <div class="form-group">
                                <div class="controls">
                                    <input id="answer" name="answer" class="form-control">
                                </div>
                            </div>

                            <input type="hidden" name="debug" value="0">

                            <div class="form-actions">
                                <input type="submit" value="Answer" class="btn btn-primary">
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>